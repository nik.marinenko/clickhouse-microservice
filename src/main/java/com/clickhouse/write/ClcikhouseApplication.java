package com.clickhouse.write;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClcikhouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClcikhouseApplication.class, args);
	}

}
