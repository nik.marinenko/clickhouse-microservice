package com.clickhouse.write.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;


@Configuration
public class ClickHouseConfig {

    @Bean
    @ConfigurationProperties("clickhouse.datasource")
    public ClickHouseProperties clickHouseProperties(){return new ClickHouseProperties();}

    @Bean
    public ClickHouseDataSource clickHouseDataSource(Environment environment, ClickHouseProperties clickHouseProperties) {
        return new ClickHouseDataSource(environment.getProperty("clickhouse.datasource.url"), clickHouseProperties);
    }

}
