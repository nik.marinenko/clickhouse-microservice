package com.clickhouse.write.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="jwt")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Validated
public class JwtConfig {
    private String secret;
    private long validity;
}