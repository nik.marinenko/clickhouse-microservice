package com.clickhouse.write.config;

import org.apache.ibatis.migration.options.DatabaseOperationOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import ru.yandex.clickhouse.ClickHouseDataSource;

@Configuration
public class MyBatisConfig {

    @Autowired
    private ClickHouseDataSource clickHouseDataSource;

    @Autowired
    private ResourceLoader resourceLoader;

}
