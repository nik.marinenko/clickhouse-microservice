package com.clickhouse.write.controller;

import com.clickhouse.write.dto.SimDTO;
import com.clickhouse.write.exception.ValidationException;
import com.clickhouse.write.services.SimService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/simcard")
@AllArgsConstructor
@Log
public class SimController {

    private final SimService simService;

    @PostMapping("/save")
    public SimDTO saveSimcard(@RequestBody SimDTO SimDTO) throws ValidationException{
        log.info("Handling save users: " + SimDTO);
        return simService.saveSimcard(SimDTO);
    }

    @GetMapping("/findAll")
    public List<SimDTO> findAllSimcard(){
        log.info("Handling find all users request");
        return simService.findAll();
    }

    @GetMapping("/findByLogin")
    public SimDTO findById(@RequestParam Long id){
        log.info("Handling find by login request: " + id);
        return simService.findBySimId(id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteSimcard(@PathVariable Long id){
        log.info("Handling delete user request: " + id);
        simService.deleteSimcard(id);
        return ResponseEntity.ok().build();
    }
}
