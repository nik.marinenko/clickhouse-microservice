package com.clickhouse.write.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimDTO {

    private Long id;

    private int accBalance;

    private String phoneNumber;

    private String token;

    private boolean active = true;

    private List<Long> operators;
}
