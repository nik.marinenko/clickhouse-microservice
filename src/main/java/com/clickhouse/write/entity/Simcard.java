package com.clickhouse.write.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ch")
@Data
@NoArgsConstructor
public class Simcard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Operator")
    private String simOperator;

    @Column(name = "Balance")
    private int accBalance;

    @Column(name = "Number")
    private String phoneNumber;

    @Column(name = "token")
    private String token;

    @Column(name = "active")
    private boolean active;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "operators",
            joinColumns = @JoinColumn(name = "operators_id"),
            inverseJoinColumns = @JoinColumn(name = "id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Operator> operators = new ArrayList<>();

    public List<Long> getOperatorId(){
        List<Long> list = new ArrayList<>();
        for (Operator one: this.operators){
            list.add(one.getId());
        }
        return list;
    }
}
