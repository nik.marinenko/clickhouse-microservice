package com.clickhouse.write.repository;


import com.clickhouse.write.entity.Simcard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface SimRepository extends JpaRepository<Simcard, Long>, JpaSpecificationExecutor<Simcard> {

    Optional<Simcard> findBySimId(Long id);

}
