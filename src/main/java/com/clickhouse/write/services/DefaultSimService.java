package com.clickhouse.write.services;

import com.clickhouse.write.dto.SimDTO;
import com.clickhouse.write.entity.Simcard;
import com.clickhouse.write.repository.SimRepository;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import javax.crypto.KeyGenerator;
import javax.transaction.Transactional;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Service
@AllArgsConstructor
@Log
public class DefaultSimService {

    private final SimRepository simRepository;

    public Iterable<Simcard> getAll(){ return simRepository.findAll(); }

    @Transactional
    public Simcard saveSimcard(SimDTO simDTO) throws Exception{
        Simcard simcard;
        if(simDTO.getId() == null){
            simcard = new Simcard();
        } else {
            simcard = simRepository.findBySimId(simDTO.getId()).orElse(null);
            if(simcard == null)
                throw new Exception("Simcard not found");
        }

        simcard.setAccBalance(simcard.getAccBalance());
        simcard.setPhoneNumber(simcard.getPhoneNumber());
        simcard.setToken(simcard.getToken());
        simcard.setActive(simcard.isActive());

        simcard = simRepository.save(simcard);

        return simRepository.save(simcard);
    }

   public void deleteSimcard(Simcard entity){simRepository.delete(entity);}

   public String generateToken() throws NoSuchAlgorithmException{
        Key key;
        SecureRandom random = new SecureRandom();
       KeyGenerator generator = KeyGenerator.getInstance("AES");
       generator.init(256, random);
       key = generator.generateKey();

       return Base64.getEncoder().encodeToString(key.getEncoded());
   }
}
