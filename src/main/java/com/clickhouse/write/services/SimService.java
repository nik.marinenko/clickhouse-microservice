package com.clickhouse.write.services;

import com.clickhouse.write.dto.SimDTO;
import com.clickhouse.write.exception.ValidationException;

import java.util.List;

public interface SimService {

    SimDTO saveSimcard (SimDTO simDTO) throws ValidationException;

    void deleteSimcard (Long id);

    SimDTO findBySimId (Long id);

    List<SimDTO> findAll();
}
