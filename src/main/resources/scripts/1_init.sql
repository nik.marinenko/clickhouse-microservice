CREATE TABLE IF NOT EXISTS ch_base.${changelog}
(
    ID UInt64,
    APPLIED_AT String,
    DESCRIPTION String
)
engine=MergeTree() ORDER BY (ID);
