create table ch_base.log_monitoring
(
    Id          String,
    simOperator    String,
    accBalance     Int32,
    createdAt      DateTime
) engine = MergeTree() ORDER BY (uuid);


